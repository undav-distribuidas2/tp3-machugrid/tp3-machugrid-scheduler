# Scheduler for machugrid

## IMPORTANT

**This environment is under construction!!**

## Submodule
- https://gitlab.com/undav-distribuidas2/tp3-machugrid/tp3-machugrid-ferri

## CURL

curl -X POST localhost:8080/app -H 'Content-type:application/json' -d '{"codeBody": "print blablabla", "parameterBody": "Soy un parametro"}'

## HTTPIE
Las consultas de prueba sobre la API fueron realizadas con HTTPIE
Para su instalación deberán realizarse los siguientes pasos:

		# Distribuciones basadas en Debian:
		apt-get install httpie

		# Distribuciones basadas en RPM:
		yum install httpie

		# Arch Linux:
		pacman -S httpie

En caso de necesitar más información puede referirse a la [documentación](https://httpie.org/doc) de HTTPIE.

