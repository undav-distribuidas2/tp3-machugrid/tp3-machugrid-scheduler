package ar.edu.undav.machugrid.scheduler;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
class AppController {

    public static class PostBody{
        public String codeBody;
        public String parameterBody;
        public String getCodeBody(){
            return codeBody;
        }
        public void setCodeBody(String codeBody){
            this.codeBody=codeBody;
        }
        public String getParameterBody() {
            return parameterBody;
        }
        public void setParameterBody(String parameterBody) {
            this.parameterBody = parameterBody;
        }
    }

    @RequestMapping(value="/app", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String post(@RequestBody PostBody body) {
        App aplication = new App(body.getCodeBody(),body.getParameterBody());


        //DO SOMETHING
        return aplication.getCode() + " " + aplication.getParameter();
    }

}