package ar.edu.undav.machugrid.scheduler;

import ar.edu.undav.machugrid.ferri.FerriConnection;
import ar.edu.undav.machugrid.ferri.FerriConnectionFactory;
import ar.edu.undav.machugrid.ferri.FerriKeyManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.ServerSocket;

@SpringBootApplication
public class SchedulerApplication {

    public static void main(String[] args) {
        new Thread(SchedulerApplication::func, "sched-node").start();

        SpringApplication.run(SchedulerApplication.class, args);
        System.out.println("Hello World ! ");
    }

    public static void func() {
        try {
            FerriKeyManager myKeyManager = new FerriKeyManager("RSA");
            FerriConnectionFactory myConnectionFactory = new FerriConnectionFactory(myKeyManager);
            //create socket
            ServerSocket serverSocket = new ServerSocket(4367);
            FerriConnection client = myConnectionFactory.listen(serverSocket);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
