package ar.edu.undav.machugrid.scheduler;

public class App {
    private String code;
    private String parameter;

    App(String code, String parameter) {
        this.code = code;
        this.parameter = parameter;
    }

    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
    public String getParameter() {
        return parameter;
    }
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

}
